# This is a File Manager
This program lets you view all the files in the resource folder and lets you filter it with by extension.
You can also choose a text file to get more information about. The project lets you search text files for a term and find out how many times that term occurs in the file. 
The project will also log all the actions that happen in regards to text files with the time it took to do those operations. 

The requirements of this project can be viewed in the pdf found in the root folder. 

![](demo.gif)

## How to run
```
$ java -jar assignment2.jar
```
