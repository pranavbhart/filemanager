import java.time.LocalDateTime;
import java.util.ArrayList;


public class Logger {

    static String logFileDir;
    private ArrayList<Log> logs = new ArrayList<>();

    public Logger(){
        String currPath = System.getProperty("user.dir");
        String sep = System.getProperty("file.separator");
        logFileDir = currPath + sep + "logs";
    }
    public Log log(){
        Log l = new Log();
        logs.add(l);
        return l;
    }

    public void logToFile(){

        for (Log l : logs){
            FileManager.writeToFile(logFileDir, "logs.txt", l.getInfo());
        }
    }


}
