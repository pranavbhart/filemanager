import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class Main {
    private static boolean running = true;
    static Menu mainMenu = new Menu("mainMenu");
    static Menu allFilesMenu = new Menu("allFilesMenu");
    static Menu extMenu = new Menu("extensionsMenu");
    static Menu maniMenu;
    static Logger logger;
    private static void init(){
        FileManager.init();
        createMainMenu();
        createAllFilesMenu();
        createExtMenu();
        logger = new Logger();
    }

    public static void main(String[] args) {
        init();

        //main loop
        while (running){
            clearScreen();
            switch(mainMenu.show()){
                case "0":
                    clearScreen();
                    allFilesMenu.show();
                    break;
                case "1":
                    //Get files by extension
                    clearScreen();
                    String index = extMenu.show();
                    ArrayList<File> files = FileManager.getFilesByExtension(Integer.parseInt(index));
                    Menu m = createSpecificExtMenu(files, "Chosen extension: ");
                    m.createPrompt("\nPress enter to go back>", null);
                    clearScreen();
                    m.show();
                    break;
                case "2":
                    clearScreen();
                    //Getting info about txt files
                    ArrayList<File> txtFiles = FileManager.getFilesByExtension("txt");
                    Menu txtFileMenu = createSpecificExtMenu(txtFiles, "Choose file: ");
                    txtFileMenu.createPrompt("\nChoose option: ");
                    int choice = Integer.parseInt(txtFileMenu.show());
                    File chosen = txtFiles.get(choice);
                    maniMenu = createManipulationMenu(chosen);
                    maniCommands(maniMenu, chosen);

                    break;
                case "3":
                    //Quit
                    logger.logToFile();
                    running = false;
                    clearScreen();
                    break;
            }
        }
    }
    //Main menu
    public static void createMainMenu(){
        mainMenu.setTitle("Welcome to File Manager");
        mainMenu.addItem("Show all files");
        mainMenu.addItem("Get files by extension");
        mainMenu.addItem("Get info of text files");
        mainMenu.addItem(Menu.ANSI_SALMON + "Quit application" + Menu.ANSI_RESET );
        mainMenu.createPrompt("\nChoose option: ");

    }
    //Menu to show all files
    public static void createAllFilesMenu(){
        allFilesMenu.setTitle("Listing all files");
        for (File f : FileManager.getAllFiles()) {
            allFilesMenu.addItem(f.getName());
        }
        allFilesMenu.createPrompt("\nPress enter to go back>", null);
    }
    //Menu to show all extensions
    public static void createExtMenu(){
        extMenu.setTitle("Extensions");
        extMenu.setItems(FileManager.getExtensions());
        extMenu.createPrompt("\nChoose Extension: ");

    }
    //Menu to show files by specific extension
    public static Menu createSpecificExtMenu(ArrayList<File> files, String title){
        Menu specExtMenu = new Menu("specExtMenu");
        for (File f : files){
            specExtMenu.addItem(f.getName());
        }
        String ext = files.get(0).getName().split("\\.")[1];
        specExtMenu.setTitle(title + ext);

        return specExtMenu;

    }

    public static Menu createManipulationMenu(File file){
        Menu menu = new Menu("txtManipulationMenu");
        menu.setTitle(file.getName());
        menu.addItem("Get size of file");
        menu.addItem("Get number of lines in file");
        menu.addItem("Check if word is in file");
        menu.addItem("Count occurrences of word in file");
        menu.addItem(Menu.ANSI_SALMON + "Back to main menu" + Menu.ANSI_RESET);
        menu.createPrompt("\nChoose option: ");
        return menu;
    }
    //Txt file manipulation loop
    public static void maniCommands(Menu menu, File file){
        Prompt enter = new Prompt("\nPress enter to go back>", null);
        boolean exit = false;
        while (!exit){
            clearScreen();
            switch (menu.show()){
                case "0":
                    //size of file
                    Log sizeLog = logger.log();
                    long size = FileManager.getSizeInKB(file);
                    String sizeResult = String.format("%s is %d kb in size.", file.getName(), size);
                    sizeLog.end("getSizeInKB", sizeResult);

                    System.out.println(sizeResult);
                    enter.showPrompt();
                    break;
                case "1":
                    //number of lines
                    Log linesLog = logger.log();
                    int lines = FileManager.getNoOfLines(file);
                    String linesResult = String.format("%s has %d number of lines.", file.getName(), lines);
                    linesLog.end("getNoOfLines", linesResult);

                    System.out.println(linesResult);
                    enter.showPrompt();
                    break;
                case "2":
                    //check for word in file
                    Prompt p = new Prompt("\nChoose what word to check for: ", null);
                    String word = p.showPrompt();
                    Log containsLog = logger.log();
                    boolean exists = FileManager.contains(file, word);
                    String result;
                    if (exists){
                        result = String.format("%s DOES contain the word '%s'.", file.getName(), word);
                    }else{
                        result = String.format("%s does NOT contain the word '%s'.", file.getName(), word);
                    }
                    System.out.println(result);
                    containsLog.end("FileManager.contains()", result);
                    enter.showPrompt();
                    break;
                case "3":
                    //count occurrences
                    Prompt p2 = new Prompt("\nChoose what word to search for: ", null);
                    String word2 = p2.showPrompt();
                    Log occLog = logger.log();
                    int occ = FileManager.occurrences(file, word2);
                    String occResult = String.format("%s contains the word '%s' %d times.", file.getName(), word2, occ);
                    occLog.end("FileManager.occurrences()", occResult);

                    System.out.println(occResult);
                    enter.showPrompt();
                    break;
                case "4":
                    exit = true;
                    break;
            }
        }
    }

    static void clearScreen() {
        try {
            if (System.getProperty("os.name").contains("Windows"))
                new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
            else
                Runtime.getRuntime().exec("clear");
        } catch (IOException | InterruptedException ex) {
            System.out.println("Error during clearScreen command!");
        }
    }

}
