import java.util.ArrayList;
import java.util.List;

public class Menu {
    private final String id;
    private String title;
    private ArrayList<String> menuItems = new ArrayList<>();
    Prompt prompt;

    public static final String ANSI_CYAN = "\u001b[36;1m";
    public static final String ANSI_RESET = "\u001b[0m";
    public static final String ANSI_SALMON = "\u001b[31;1m";

    public Menu(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void addItem(String item) {
        this.menuItems.add(item);
    }

    public void setItems(ArrayList<String> items){
        this.menuItems = items;
    }

    public boolean contains(String item){
        return this.menuItems.contains(item);
    }

    //auto creates accepted inputs
    public void createPrompt(String msg){
        ArrayList<String> acceptedInputs = new ArrayList<>();
        for (int i = 0; i < menuItems.size(); i++) {
            acceptedInputs.add(Integer.toString(i));
        }
        this.prompt = new Prompt(msg, acceptedInputs);
    }
    //overloaded method that has acceptedInputs as parameter
    public void createPrompt(String msg, List<String> acceptedInputs){
        this.prompt = new Prompt(msg, acceptedInputs);
    }

    private String showPrompt(){
        return this.prompt.showPrompt();
    }


    private void showMenuItems(){
        System.out.println(ANSI_CYAN+title+ANSI_RESET);
        System.out.println("------------------------------");
        for (int i = 0; i < menuItems.size(); i++) {
            System.out.printf("%s%d. %s", ANSI_CYAN, i, ANSI_RESET);
            System.out.println(menuItems.get(i));
        }
    }

    public String show(){
        showMenuItems();
        return showPrompt();

    }
}
