import java.util.*;
import java.io.*;

public class FileManager{

    private final static ArrayList<String> extensions = new ArrayList<>();
    private static File[] files;

    public static void init(){
        initFiles();
        initExtensions();
    }

    private static void initFiles(){
        String path = System.getProperty("user.dir");
        String sep = System.getProperty("file.separator");
        String resourcesPath = path + sep + "src" + sep + "main" + sep + "resources";

        try{
            File resourceFolder = new File(resourcesPath);
            files = resourceFolder.listFiles();
        }
        catch(Exception e){
            System.out.println("Something went wrong");
        }
    }

    private static void initExtensions(){
        //adds extensions to array
        for (File f : files){
            if (f.getName().contains(".")){
                String ext = f.getName().split("\\.")[1];
                if (!extensions.contains(ext)){
                    extensions.add(ext);
                }
            }
        }
    }

    public static ArrayList<File> getFilesByExtension(String ext){
        ArrayList<File> ret = new ArrayList<>();
        for (File f : files){
            if (f.getName().endsWith(ext)){
                ret.add(f);
            }
        }
        return ret;
    }

    public static ArrayList<File> getFilesByExtension(int index){
        ArrayList<File> ret = new ArrayList<>();
        String ext = extensions.get(index);
        for (File f : files){
            if (f.getName().endsWith(ext)){
                ret.add(f);
            }
        }
        return ret;
    }

    public static File[] getAllFiles(){
        return files;
    }

    public static ArrayList<String> getExtensions(){
        return extensions;
    }

    public static long getSizeInKB(File file){
        return file.length() / 1024;
    }

    public static int getNoOfLines(File file){
        int count = 0;
        try{
            Scanner fileScanner = new Scanner(file);
            while(fileScanner.hasNextLine()){
                fileScanner.nextLine();
                count++;
            }
        }catch(Exception e){
            System.out.println("Error while reading file!");
        }
        return count;
    }

    public static boolean contains(File file, String word){
        try{
            Scanner fileScanner = new Scanner(file);
            while(fileScanner.hasNextLine()){
                String line = fileScanner.nextLine().toLowerCase();
                if (line.contains(word.toLowerCase()))
                    return true;
            }
        }catch(Exception e){
            System.out.println("Error while reading file!");
        }
        return false;
    }

    public static int occurrences(File file, String word){
        word = word.toLowerCase();
        int count = 0;
        try{
            Scanner fileScanner = new Scanner(file);
            while(fileScanner.hasNextLine()){
                String line = fileScanner.nextLine().toLowerCase().replaceAll("[^a-z']", " ");
                String[] words = line.split("\\s+");

                for(String w : words){
                    if (w.equals(word)){
                        count++;
                    }
                }
            }
        }catch(Exception e){
            System.out.println("Error while reading file!");
        }
        return count;
    }


    public static void writeToFile(String dirPath, String fileName, String msg){
        String sep = System.getProperty("file.separator");
        //creates directories and file if it doesn't exist and writes to the end.
        try{
            File dir = new File(dirPath);
            if (!dir.exists()) dir.mkdirs();
            String filePath = dir + sep + fileName;
            File f = new File(filePath);
            if (!f.exists()) f.createNewFile();

            Writer wr = new FileWriter(f, true);
            wr.write(msg + "\n");
            wr.flush();
            wr.close();

        }catch(Exception e){
            System.out.println("Error during file writing.");
        }

    }

}


