import java.util.List;
import java.util.Scanner;

public class Prompt {
    static Scanner scanner = new Scanner(System.in);
    private final String promptMsg;
    private final List<String> acceptedInputs;

    public Prompt(String promptMsg, List<String> acceptedInputs) {
        this.promptMsg = promptMsg;
        this.acceptedInputs = acceptedInputs;
    }

    public String showPrompt(){

        if (acceptedInputs == null){ //when any response is accepted
            System.out.print(promptMsg);
            return scanner.nextLine();
        }
        String input = null;
        while(!acceptedInputs.contains(input)){
            System.out.print(promptMsg);
            input = scanner.nextLine();
            if (!acceptedInputs.contains(input)){
                System.out.println("Please provide one of the options above!(number)");
            }
        }
        return input;
    }

}
