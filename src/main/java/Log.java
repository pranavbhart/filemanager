import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


public class Log{
    LocalDateTime startTime;
    LocalDateTime endTime;
    String methodUsed;
    String result;

    protected Log(){
        startTime = LocalDateTime.now();

    }

    protected void end(String methodUsed, String result){
        endTime = LocalDateTime.now();
        this.methodUsed = methodUsed;
        this.result = result;
    }

    protected String getInfo(){
        long duration = Duration.between(startTime, endTime).toMillis();
        String endTimeFormatted = endTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        return String.format("%s: %s The function took %d ms to execute.", endTimeFormatted, result, duration);
    }
}